package service

import (
	"app/config"
	"app/genproto/tovar_service"
	"app/grpc/client"
	"app/models"
	"app/pkg/logger"
	"app/storage"
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ProductService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*tovar_service.UnimplementedProductServiceServer
}

func NewProductService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *ProductService {
	return &ProductService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *ProductService) Create(ctx context.Context, req *tovar_service.CreateProduct) (resp *tovar_service.Product, err error) {

	i.log.Info("---CreateProduct------>", logger.Any("req", req))

	pKey, err := i.strg.Product().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateProduct->Product->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Product().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyProduct->Product->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ProductService) GetByID(ctx context.Context, req *tovar_service.ProductPrimaryKey) (resp *tovar_service.Product, err error) {

	i.log.Info("---GetUserByID------>", logger.Any("req", req))

	resp, err = i.strg.Product().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetUserByID->User->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ProductService) GetList(ctx context.Context, req *tovar_service.GetListProductRequest) (resp *tovar_service.GetListProductResponse, err error) {

	i.log.Info("---GetCategories------>", logger.Any("req", req))

	resp, err = i.strg.Product().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetCategories->Product->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ProductService) Update(ctx context.Context, req *tovar_service.UpdateProduct) (resp *tovar_service.Product, err error) {

	i.log.Info("---UpdateProduct------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Product().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateProduct--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Product().GetByPKey(ctx, &tovar_service.ProductPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetProduct->Product->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *ProductService) UpdatePatch(ctx context.Context, req *tovar_service.UpdatePatchProduct) (resp *tovar_service.Product, err error) {

	i.log.Info("---UpdatePatchProduct------>", logger.Any("req", req))

	updatePatchModel := models.UpdatePatchRequest{
		Id:     req.GetId(),
		Fields: req.GetFields().AsMap(),
	}

	rowsAffected, err := i.strg.Product().UpdatePatch(ctx, &updatePatchModel)

	if err != nil {
		i.log.Error("!!!UpdatePatchProduct--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Product().GetByPKey(ctx, &tovar_service.ProductPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetProduct->Product->Get--->", logger.Error(err))

		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *ProductService) Delete(ctx context.Context, req *tovar_service.ProductPrimaryKey) (resp *empty.Empty, err error) {

	i.log.Info("---DeleteProduct------>", logger.Any("req", req))

	err = i.strg.Product().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteProduct->Product->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
