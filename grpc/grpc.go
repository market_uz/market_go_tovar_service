package grpc

import (
	"app/config"
	"app/genproto/tovar_service"
	"app/grpc/client"
	"app/grpc/service"
	"app/pkg/logger"
	"app/storage"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {

	grpcServer = grpc.NewServer()

	tovar_service.RegisterCategoryServiceServer(grpcServer, service.NewUserService(cfg, log, strg, srvc))
	tovar_service.RegisterProductServiceServer(grpcServer, service.NewProductService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)
	return
}
