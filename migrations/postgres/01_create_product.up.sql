CREATE TABLE IF NOT EXISTS "products" (
    "id" UUID PRIMARY KEY,
    "barcode" VARCHAR(50) NOT NULL,
    "name" VARCHAR(50) NOT NULL,
    "price" NUMERIC NOT NULL,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);