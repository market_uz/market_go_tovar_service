CREATE TABLE IF NOT EXISTS "categories" (
    "id" UUID PRIMARY KEY,
    "category_name" VARCHAR(50) NOT NULL,
    "parent_id" UUID REFERENCES categories(id) NULL,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);