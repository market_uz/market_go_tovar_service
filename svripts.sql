SELECT 
    c.id,
    c.category_name,
    COALESCE(c.parent_id, NULL),
    COALESCE(cp.category_name, ''),
    c.created_at, 
    c.updated_at
FROM categories c
LEFT JOIN categories cp ON c.parent_id = cp.id
WHERE c.id = $1
