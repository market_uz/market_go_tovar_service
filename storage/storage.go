package storage

import (
	"app/genproto/tovar_service"
	"app/models"
	"context"
)

type StorageI interface {
	CloseDB()
	Category() CategoryRepoI
	Product() ProductRepoI
}

type CategoryRepoI interface {
	Create(ctx context.Context, req *tovar_service.CreateCategory) (resp *tovar_service.CategoryPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *tovar_service.CategoryPrimaryKey) (resp *tovar_service.Category, err error)
	GetAll(ctx context.Context, req *tovar_service.GetListCategoryRequest) (resp *tovar_service.GetListCategoryResponse, err error)
	Update(ctx context.Context, req *tovar_service.UpdateCategory) (rowsAffected int64, err error)
	UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *tovar_service.CategoryPrimaryKey) error
}

type ProductRepoI interface {
	Create(ctx context.Context, req *tovar_service.CreateProduct) (resp *tovar_service.ProductPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *tovar_service.ProductPrimaryKey) (resp *tovar_service.Product, err error)
	GetAll(ctx context.Context, req *tovar_service.GetListProductRequest) (resp *tovar_service.GetListProductResponse, err error)
	Update(ctx context.Context, req *tovar_service.UpdateProduct) (rowsAffected int64, err error)
	UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *tovar_service.ProductPrimaryKey) error
}
