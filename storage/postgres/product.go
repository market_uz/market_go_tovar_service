package postgres

import (
	"app/genproto/tovar_service"
	"app/models"
	"app/pkg/helper"
	"context"
	"database/sql"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type productRepo struct {
	db *pgxpool.Pool
}

func NewProductRepo(db *pgxpool.Pool) *productRepo {
	return &productRepo{
		db: db,
	}
}

func (p *productRepo) Create(ctx context.Context, req *tovar_service.CreateProduct) (resp *tovar_service.ProductPrimaryKey, err error) {
	id := uuid.New().String()
	query := `INSERT INTO "products" (
			id,
            barcode,
        	name,
        	price,
        	updated_at
        ) VALUES ($1, $2, $3, $4, now())
    `
	_, err = p.db.Exec(ctx,
		query,
		id,
		req.ProductBarcode,
		req.ProductName,
		req.ProductPrice,
	)

	if err != nil {
		return nil, err
	}

	return &tovar_service.ProductPrimaryKey{Id: id}, nil
}

func (p *productRepo) GetByPKey(ctx context.Context, req *tovar_service.ProductPrimaryKey) (resp *tovar_service.Product, err error) {
	query := `
    	SELECT
			id,
      		barcode,
      		name,
      		price,
      		created_at,
      		updated_at
    	FROM "products" 
    	WHERE id = $1
    `

	var (
		id        sql.NullString
		barcode   sql.NullString
		name      sql.NullString
		price     sql.NullFloat64
		createdAt sql.NullString
		updatedAt sql.NullString
	)

	err = p.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&barcode,
		&name,
		&price,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &tovar_service.Product{
		Id:             id.String,
		ProductBarcode: barcode.String,
		ProductName:    name.String,
		ProductPrice:   price.Float64,
		CreatedAt:      createdAt.String,
		UpdatedAt:      updatedAt.String,
	}

	return
}

func (p *productRepo) GetAll(ctx context.Context, req *tovar_service.GetListProductRequest) (resp *tovar_service.GetListProductResponse, err error) {
	resp = &tovar_service.GetListProductResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
	  	SELECT
			COUNT(*) OVER(),
			id,
			barcode,
			name,
			price,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
	 	FROM "products"
	`

	if len(req.GetSearch()) > 0 {
		filter += " AND barcode ILIKE '%' || '" + req.Search + "' || '%' "
	}
	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}
	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := p.db.Query(ctx, query, args...)

	if err != nil {
		return resp, err
	}
	defer rows.Close()

	for rows.Next() {
		var (
			id        sql.NullString
			barcode   sql.NullString
			name      sql.NullString
			price     sql.NullFloat64
			createdAt sql.NullString
			updatedAt sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&barcode,
			&name,
			&price,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Products = append(resp.Products, &tovar_service.Product{
			Id:             id.String,
			ProductBarcode: barcode.String,
			ProductName:    name.String,
			ProductPrice:   price.Float64,
			CreatedAt:      createdAt.String,
			UpdatedAt:      updatedAt.String,
		})
	}

	return
}

func (p *productRepo) Update(ctx context.Context, req *tovar_service.UpdateProduct) (rowsAffected int64, err error) {
	var (
		query  string
		params map[string]interface{}
	)

	query = `
		  UPDATE
			  "products"
		  SET
		  	barcode = :barcode,
			name = :name,
			price = :price,
			updated_at = now()
		  WHERE
			id = :id`
	params = map[string]interface{}{
		"id":      req.GetId(),
		"barcode": req.GetProductBarcode(),
		"name":    req.GetProductName(),
		"price":   req.GetProductPrice(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := p.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (p *productRepo) UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error) {
	var (
		set   = " SET "
		ind   = 0
		query string
	)

	if len(req.Fields) == 0 {
		err = errors.New("no updates provided")
		return
	}

	req.Fields["id"] = req.Id

	for key := range req.Fields {
		set += fmt.Sprintf(" %s = :%s ", key, key)
		if ind != len(req.Fields)-1 {
			set += ", "
		}
		ind++
	}

	query = `
		UPDATE
			"products"
	` + set + ` , updated_at = now()
		WHERE
			id = :id
	`

	fmt.Println(query)

	query, args := helper.ReplaceQueryParams(query, req.Fields)

	result, err := p.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), err
}

func (p *productRepo) Delete(ctx context.Context, req *tovar_service.ProductPrimaryKey) error {
	query := `DELETE FROM "products" WHERE id = $1`

	_, err := p.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
