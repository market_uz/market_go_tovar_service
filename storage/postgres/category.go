package postgres

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"app/genproto/tovar_service"
	"app/models"
	"app/pkg/helper"
)

type categoryRepo struct {
	db *pgxpool.Pool
}

func NewCategoryRepo(db *pgxpool.Pool) *categoryRepo {
	return &categoryRepo{
		db: db,
	}
}

func (c *categoryRepo) Create(ctx context.Context, req *tovar_service.CreateCategory) (resp *tovar_service.CategoryPrimaryKey, err error) {
	var id = uuid.New().String()

	query := `INSERT INTO "categories" (
			id, 
			category_name,
			parent_id, 
			updated_at
		) VALUES ($1, $2, $3, NOW())
	`

	_, err = c.db.Exec(ctx,
		query,
		id,
		req.CategoryName,
		helper.NewNullString(req.ParentId),
	)
	if err != nil {
		return nil, err
	}

	return &tovar_service.CategoryPrimaryKey{Id: id}, nil
}

func (c *categoryRepo) GetByPKey(ctx context.Context, req *tovar_service.CategoryPrimaryKey) (resp *tovar_service.Category, err error) {
	query := `
		SELECT 
    		c.id,
    		c.category_name,
    		COALESCE(c.parent_id, NULL),
    		COALESCE(cp.category_name, ''),
    		c.created_at, 
    		c.updated_at
		FROM categories c
		LEFT JOIN categories cp ON c.parent_id = cp.id
		WHERE c.id = $1
	`

	var (
		id            sql.NullString
		category_name sql.NullString
		parent_id     sql.NullString
		parent_name   sql.NullString
		created_at    sql.NullString
		updated_at    sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&category_name,
		&parent_id,
		&parent_name,
		&created_at,
		&updated_at,
	)
	if err != nil {
		return nil, err
	}

	resp = &tovar_service.Category{
		Id:           id.String,
		CategoryName: category_name.String,
		CreatedAt:    created_at.String,
		UpdatedAt:    updated_at.String,
	}

	if parent_id.Valid && parent_id.String != "" {
		resp.ParentId = parent_id.String
	}

	if parent_name.Valid && parent_name.String != "" {
		resp.PatentName = parent_name.String
	}

	return
}

func (c *categoryRepo) GetAll(ctx context.Context, req *tovar_service.GetListCategoryRequest) (resp *tovar_service.GetListCategoryResponse, err error) {
	resp = &tovar_service.GetListCategoryResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE "
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT 
  			count(*) over(),
  			c.id,
  			c.category_name,
  			c.parent_id,
  			COALESCE(cp.category_name, ''),
  			(
    			WITH RECURSIVE parent_categories AS (
      			SELECT id, parent_id, category_name
      			FROM categories
      			WHERE id = c.id
      			UNION
      			SELECT c.id, c.parent_id, c.category_name
      			FROM categories c
      			JOIN parent_categories pc ON pc.parent_id = c.id
    			)
    			SELECT id
    			FROM parent_categories
    			WHERE parent_id IS NULL
    			LIMIT 1
  			) AS brand_id,
  			(
    			WITH RECURSIVE parent_categories AS (
      			SELECT id, parent_id, category_name
      			FROM categories
      			WHERE id = c.id
      			UNION
      			SELECT c.id, c.parent_id, c.category_name
      			FROM categories c
      			JOIN parent_categories pc ON pc.parent_id = c.id
    			)
    			SELECT category_name
    			FROM categories p
    			WHERE p.id = (
      			SELECT id
      			FROM parent_categories
      			WHERE parent_id IS NULL
      			LIMIT 1
    			)
  			) AS brand_name,
  			c.created_at, 
  			c.updated_at
			FROM categories c
			LEFT JOIN categories cp ON c.parent_id = cp.id
		`

	if len(req.GetSearch()) > 0 {
		filter += " AND category_name ILIKE '%' || '" + req.Search + "' || '%' "
	}
	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}
	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)

	if err != nil {
		return resp, err
	}
	defer rows.Close()

	for rows.Next() {
		var (
			id            sql.NullString
			category_name sql.NullString
			parent_id     sql.NullString
			parent_name   sql.NullString
			brand_id      sql.NullString
			brand_name    sql.NullString
			created_at    sql.NullString
			updated_at    sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&category_name,
			&parent_id,
			&parent_name,
			&brand_id,
			&brand_name,
			&created_at,
			&updated_at,
		)
		if err != nil {
			return resp, err
		}

		resp.Categories = append(resp.Categories, &tovar_service.Category{
			Id:           id.String,
			CategoryName: category_name.String,
			ParentId:     parent_id.String,
			PatentName:   parent_name.String,
			BrandId:      brand_id.String,
			BrandName:    brand_name.String,
			CreatedAt:    created_at.String,
			UpdatedAt:    updated_at.String,
		})
	}

	return
}

func (c *categoryRepo) Update(ctx context.Context, req *tovar_service.UpdateCategory) (rowsAffected int64, err error) {
	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE 
			"categories"
		SET 
			category_name = :category_name,
			parent_id = :parent_id,
			updated_at = now()
		WHERE 
			id = :id
	`
	params = map[string]interface{}{
		"category_name": req.GetCategoryName(),
		"parent_id":     req.GetParentId(),
		"id":            req.GetId(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *categoryRepo) UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error) {
	var (
		set   = " SET "
		ind   = 0
		query string
	)

	if len(req.Fields) == 0 {
		err = errors.New("no updates provided")
		return
	}

	req.Fields["id"] = req.Id

	for key := range req.Fields {
		set += fmt.Sprintf(" %s = :%s ", key, key)
		if ind != len(req.Fields)-1 {
			set += ", "
		}
		ind++
	}
	
	query = `
		UPDATE
			"categories"
	` + set + ` , updated_at = now()
		WHERE
			id = :id
	`

	query, args := helper.ReplaceQueryParams(query, req.Fields)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), err
}

func (c *categoryRepo) Delete(ctx context.Context, req *tovar_service.CategoryPrimaryKey) error {
	query := `DELETE FROM "categories" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)
	if err != nil {
		return err
	}

	return nil
}
